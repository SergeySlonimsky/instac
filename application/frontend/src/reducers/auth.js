import {AUTH_LOGIN, AUTH_LOGOUT} from "../actions/AuthActions";

const initialState = {
    authenticated: false
};

export function authReducer(state = initialState, action) {
    switch (action.type) {
        case AUTH_LOGIN:
            return {...state, authenticated: true};

        case AUTH_LOGOUT:
            return {...state, authenticated: false};

        default:
            return state
    }
}

import {
    LOADER_ENABLE,
    LOADER_DISABLE
} from '../actions/LoaderActions'

const initialState = {
    loading: false
};

export function loaderReducer(state = initialState, action) {
    switch (action.type) {
        case LOADER_ENABLE:
            return {...state, loading: true};

        case LOADER_DISABLE:
            return {...state, loading: false};
        default:
            return state
    }
}

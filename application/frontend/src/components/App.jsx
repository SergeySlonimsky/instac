import React, {Component} from 'react'
import {Route, Link} from 'react-router-dom'
import {RequireAuthenticated, RequireAnon} from "./Auth/RequireAuth";
import User from "./User";
import About from "./About";
import Auth from "../containers/Auth";

class App extends Component {
    render() {
        return <div>
            <header>
                <Link to="/">User</Link>
                <Link to="/about-us">About</Link>
                <Link to="/auth">Auth</Link>
            </header>

            <main>
                <Route exact path="/" component={RequireAnon(User)}/>
                <Route exact path="/about-us" component={RequireAuthenticated(About)}/>
                <Route exact path="/auth" component={Auth}/>
            </main>
        </div>
    }
}

export default App

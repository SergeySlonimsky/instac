export const LOADER_ENABLE = 'LOADER_ENABLE';
export const LOADER_DISABLE = 'LOADER_DISABLE';

export function handleLoaderEnable() {
    return function (dispatch) {
        dispatch({
            type: LOADER_ENABLE,
        });
    }
}

export function handleLoaderDisable() {
    return function (dispatch) {
        dispatch({
            type: LOADER_DISABLE,
        });
    }
}

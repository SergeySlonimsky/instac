export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAIL = 'LOGIN_FAIL';

export function handle() {
    return function (dispatch) {
        dispatch({
            type: LOGIN_REQUEST,
        });

        fetch('http://localhost/auth/login', {
            method: 'POST',
            body: JSON.stringify({
                username: "testuser",
                password: "123456"
            }),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => {
                if (!response.ok) {
                    throw response.json();
                }

                return response.json();
            })
            .then( data => {
                dispatch({
                    type: LOGIN_SUCCESS,
                    payload: data.data.token,
                });
            })
            .catch( promise => {
                Promise.resolve(promise).then(error => {
                    dispatch({
                        type: LOGIN_FAIL,
                        payload: error.message,
                    });
                })
            })
    }
}

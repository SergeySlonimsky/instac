export const AUTH_LOGIN = 'AUTH_LOGIN';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';

export function handleLogin() {
    return function (dispatch) {
        console.log('login');
        dispatch({
            type: AUTH_LOGIN
        })
    }
}

export function handleLogout() {
    return function (dispatch) {
        console.log('logout');
        dispatch({
            type: AUTH_LOGOUT
        })
    }
}

import React from 'react'
import {connect} from 'react-redux'
import {handleLogin, handleLogout} from '../actions/AuthActions'

class AuthContainer extends React.Component {

    render() {
        const {auth} = this.props;
        return (
            <div>
                <p>Authenticated: {auth.authenticated ? 'Yes' : 'No'}</p>
                <button onClick={this.props.login}>Login</button>
                <button onClick={this.props.logout}>Logout</button>
            </div>
        )
    }
}

const mapStateToProps = store => {
    return {
        auth: store.auth,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        login: () => dispatch(handleLogin()),
        logout: () => dispatch(handleLogout()),
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthContainer)
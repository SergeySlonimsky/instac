import React from 'react'
import {connect} from 'react-redux'
import User from '../components/User'
import {handleLogin} from '../actions/UserActions'

class UserContainer extends React.Component {
    handleLogin = () => {
        const {handleLogin} = this.props;
        handleLogin();
    };

    render() {
        const {user} = this.props;
        return (
            <User
                name={user.name}
                error={user.error}
                isFetching={user.isFetching}
                handleLogin={this.handleLogin}
            />
        )
    }
}

const mapStateToProps = store => {
    return {
        user: store.user,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        handleLogin: successCallback => dispatch(handleLogin(successCallback)),
    }
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserContainer)
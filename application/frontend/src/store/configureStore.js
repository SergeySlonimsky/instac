import {createStore, applyMiddleware, compose} from 'redux'
import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import {routerMiddleware} from 'connected-react-router'
import createRootReducer from './../reducers'

export const history = createHistory();

const enhancers = [];
const middleware = [thunk, routerMiddleware(history)];

const composedEnhancers = compose(
    applyMiddleware(...middleware),
    ...enhancers
);

export default createStore(
    createRootReducer(history),
    composedEnhancers
)

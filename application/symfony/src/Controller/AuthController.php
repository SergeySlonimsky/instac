<?php

namespace App\Controller;

use App\Form\LoginFormType;
use App\Form\RegisterFormType;
use App\Service\Api\ApiResponseError;
use App\Service\Api\Exception\ApiException;
use App\Service\Auth\AuthProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class AuthController
 * @package App\Controller
 * @Route(path="/auth")
 */
class AuthController extends ApiController
{
    /**
     * @param Request $request
     * @return Response
     *
     * @Route(
     *     "/login",
     *     name="auth_login",
     *     methods={"POST"}
     * )
     */
    public function login(Request $request): Response
    {
        $form = $this
            ->createForm(LoginFormType::class)
            ->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('login_check', $request->request->all(), Response::HTTP_TEMPORARY_REDIRECT);
        }

        $error = new ApiResponseError(
            ApiResponseError::ERROR_TYPE_VALIDATION,
            $form->getName(),
            'validation failed'
        );

        foreach ($form->getErrors(true) as $formError) {
            $error->addInner(new ApiResponseError(
                ApiResponseError::ERROR_TYPE_VALIDATION,
                $formError->getOrigin()->getName(),
                $formError->getMessage()
            ));
        }

        return $this->response($error, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param Request $request
     * @param AuthProvider $provider
     * @return Response
     *
     * @Route(
     *     "/register",
     *     name="auth_register",
     *     methods={"POST"}
     * )
     */
    public function register(Request $request, AuthProvider $provider): Response
    {
        $form = $this
            ->createForm(RegisterFormType::class)
            ->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $id = $provider->register($form->getData());
            } catch (ApiException $e) {
                $error = new ApiResponseError(
                    ApiResponseError::ERROR_TYPE_BAD_REQUEST,
                    $form->getName(),
                    $e->getMessage()
                );

                return $this->response($error, Response::HTTP_BAD_REQUEST);
            }

            return $this->response($id);
        }

        $error = new ApiResponseError(
            ApiResponseError::ERROR_TYPE_VALIDATION,
            $form->getName(),
            'validation failed'
        );

        foreach ($form->getErrors(true) as $formError) {
            $error->addInner(new ApiResponseError(
                ApiResponseError::ERROR_TYPE_VALIDATION,
                $formError->getOrigin()->getName(),
                $formError->getMessage()
            ));
        }

        return $this->response($error, Response::HTTP_BAD_REQUEST);
    }
}

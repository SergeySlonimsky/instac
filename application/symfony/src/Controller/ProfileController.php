<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AuthController
 * @package App\Controller
 * @Route(path="/api")
 */
class ProfileController extends ApiController
{
    /**
     * @param Request $request
     * @return Response
     *
     * @Route(
     *     "/profile",
     *     name="profile_profile",
     *     methods={"GET"}
     * )
     */
    public function profile(): Response
    {
        return $this->response(
            $this->getUser()->getUsername()
        );
    }
}
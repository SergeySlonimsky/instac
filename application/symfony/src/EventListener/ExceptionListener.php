<?php

namespace App\EventListener;

use App\Service\Api\ApiResponseError;
use App\Service\Api\ApiResponseInterface;
use App\Service\Api\ApiResponseService;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionListener implements EventSubscriberInterface
{
    /**
     * @var ApiResponseInterface
     */
    private $response;

    public function __construct(ApiResponseService $response)
    {
        $this->response = $response;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::JWT_NOT_FOUND => [
                ['processInvalidJWT'],
            ],
            Events::JWT_INVALID => [
                ['processInvalidJWT'],
            ],
            KernelEvents::EXCEPTION => [
                ['processException'],
            ],

        ];
    }

    public function processException(GetResponseForExceptionEvent $event): void
    {
        $exception = $event->getException();
        $errorResponse = $this->response->response(
            new ApiResponseError(ApiResponseError::ERROR_TYPE_INTERNAL, 'internal', $exception->getMessage()),
            Response::HTTP_INTERNAL_SERVER_ERROR
        );

        $event->setResponse($errorResponse);
    }

    public function processInvalidJWT(AuthenticationFailureEvent $event): void
    {
        $exception = $event->getException();
        $errorResponse = $this->response->response(
            new ApiResponseError(ApiResponseError::ERROR_TYPE_BAD_REQUEST, 'auth', $exception->getMessage()),
            Response::HTTP_UNAUTHORIZED
        );

        $event->setResponse($errorResponse);
    }
}

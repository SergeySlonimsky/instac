<?php

namespace App\Service\Auth\Handler;

use App\Service\Api\ApiResponseError;
use App\Service\Api\ApiResponseInterface;
use App\Service\Api\ApiResponseService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

class FailureHandler implements AuthenticationFailureHandlerInterface
{
    /**
     * @var ApiResponseInterface
     */
    private $response;

    public function __construct(ApiResponseService $response)
    {
        $this->response = $response;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return $this->response->response(
            new ApiResponseError(ApiResponseError::ERROR_TYPE_AUTH, 'login_form', $exception->getMessage()),
            Response::HTTP_BAD_REQUEST
        );
    }
}
<?php

namespace App\Service\Auth\Handler;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;

class SuccessHandler implements AuthenticationSuccessHandlerInterface
{
    /**
     * @var JWTTokenManagerInterface
     */
    protected $jwtManager;

    /**
     * @param JWTTokenManagerInterface $jwtManager
     */
    public function __construct(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }

    /**
     * {@inheritdoc}
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        /** @var User $user */
        $user = $token->getUser();
        if (!$user->isActive()) {
            return new JsonResponse(
                [
                    'error' => [
                        'target' => 'login',
                        'message' => 'This user is inactive',
                    ],
                ],
                Response::HTTP_BAD_REQUEST
            );
        }

        $jwt = $this->jwtManager->create($user);

        return new JsonResponse([
            'data' => [
                'token' => $jwt,
            ],
        ]);
    }
}

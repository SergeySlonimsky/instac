<?php

namespace App\Service\Api;

use Symfony\Component\HttpFoundation\Response;

interface ApiResponseInterface
{
    public function response($data, int $status): Response;
}
